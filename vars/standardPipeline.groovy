#!groovy

def call() {
    pipeline {
        agent {
            kubernetes {
                defaultContainer 'maven'
                yaml '''
                    spec:
                        containers:
                        - name: maven
                          image: maven:3.5.0-jdk-8
                          resources:
                            limits:
                              cpu: 1
                              memory: 4Gi
                            requests:
                              cpu: 514m
                              memory: 3Gi
                    '''
            }
        }

        stages {
            stage('Build') {
                steps {
                    sh "mvn -version"
                    sh 'java -version'
                    sh 'bash pipelines-maven.sh'
                    sh 'mvn -s settings.xml -U dependency:purge-local-repository -DskipTests clean package'
                }
            }
            stage('Test') {
                steps {
                    sh 'mvn -s settings.xml test'
                }
                post {
                    always {
                        junit '**/surefire-reports/*.xml'
                    }
                }
            }
        }
    }
}
