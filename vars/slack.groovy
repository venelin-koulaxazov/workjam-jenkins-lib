#!groovy
def success() {
    slackSend(color: '#00FF00',
            message: 'Build successful',
            tokenCredentialId: 'slack-token',
            channel: '@U01BBJJ3LRH')
}

def failure() {
    slackSend(color: '#FF0000',
            message: 'Build failed',
            tokenCredentialId: 'slack-token',
            channel: '@U01BBJJ3LRH')
}
