# WorkJam Jenkins Lib #

This repository is a shared jenkins library which provides helper functions
to build, test and deploy microservices. It also contains various util functions used
for notifications in Slack, JIRA, etc.
